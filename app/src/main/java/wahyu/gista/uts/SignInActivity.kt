package wahyu.gista.uts

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity  :  AppCompatActivity(), View.OnClickListener {
    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnTgs.setOnClickListener(this)
        btnLogOff.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnTgs -> {
                var intent = Intent(this, CustomAdapter::class.java)

                startActivity(Intent())
            }
            R.id.btnLogOff -> {
                var intentIden = Intent(this, MainActivity::class.java)
                startActivity(Intent())
            }
        }
    }
}