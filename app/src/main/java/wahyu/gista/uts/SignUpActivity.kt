package wahyu.gista.uts

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        var email = edRegUserName.text.toString()
        var password = edRegPassword.text.toString()

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Username / password tidak boleh kosong", Toast.LENGTH_SHORT).show()
        }else{
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Tunggu sebentar...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this, "Registrasi Berhasil", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this,
                        "Username/password Salah", Toast.LENGTH_SHORT).show()
                }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        btnRegister.setOnClickListener(this)
    }



}