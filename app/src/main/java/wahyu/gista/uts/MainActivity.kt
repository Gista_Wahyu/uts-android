package wahyu.gista.uts

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin -> {
                var email = edUserName.text.toString()
                var password = edPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "Username / password tidak boleh kosong", Toast.LENGTH_SHORT).show()
                }else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Tunggu sebentar...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                            .addOnCompleteListener {
                                progressDialog.hide()
                                if (!it.isSuccessful) return@addOnCompleteListener
                                Toast.makeText(this, "Login Berhasil", Toast.LENGTH_SHORT).show()
                                val intent = Intent ( this, SignInActivity::class.java)
                                startActivity(intent)
                            }
                            .addOnCompleteListener {
                                progressDialog.hide()
                                Toast.makeText(this,
                                        "Username/password Salah", Toast.LENGTH_SHORT).show()
                            }
                }
            }
            R.id.txSignUp ->{
                var intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin.setOnClickListener(this)
        txSignUp.setOnClickListener(this)
    }
}